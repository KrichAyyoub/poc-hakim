import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';


export interface truc {
  message : string
}

@Injectable({
  providedIn: 'root'
})
export class PlotServiceService {

  constructor(private httpClient : HttpClient) { }

  generatePlot(id : string) : Observable<truc>{
    return this.httpClient.get<truc>(`http://localhost:5000/generatePlot/${id}`)
  }
}
